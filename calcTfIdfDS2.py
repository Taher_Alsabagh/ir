import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import joblib
import sys
import numpy as np

# قراءة الـ dataset
dataset = pd.read_csv("DS2Files/df_docs_preprocessed.csv")

docs_data = pd.DataFrame({
    'doc_id': dataset['doc_id'],
    'new_text': dataset['new_text'],
}, index=range(len(dataset)))

df_docs = pd.DataFrame(docs_data)

# Calculate TF-IDF
tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(df_docs['new_text'].values.astype('U'))

# Create a dictionary to map row index to doc_id
doc_id_map = {i: doc_id for i, doc_id in enumerate(df_docs['doc_id'])}
print(doc_id_map)

# Save TF-IDF Vectorizer using joblib
joblib.dump(tfidf_vectorizer, 'DS2Files/dataset2_tfidf_vectorizer/tfidf_vectorizer.pkl')

# Save TF-IDF Matrix
joblib.dump(tfidf_matrix, 'DS2Files/dataset2_tfidf_vectorizer/tfidf_matrix.pkl')

# Save doc_id map
joblib.dump(doc_id_map, 'DS2Files/dataset2_tfidf_vectorizer/doc_id_map.pkl')

print('Successfully end')
