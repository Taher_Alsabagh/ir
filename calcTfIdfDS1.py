import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import joblib
from nltk.tokenize import word_tokenize

# قراءة الـ dataset
documents = pd.read_csv("DS1Files/df_docs_preprocessed.csv")
# documents = joblib.load('DS1Files/df_docs_preprocessed.pkl')

# الحصول على معرّفات الوثائق والنصوص
document_ids = documents['doc_id'].tolist()
# print(document_ids)
documents = documents['new_text'].tolist()


# tokenized_documents = [word_tokenize(doc.lower()) for doc in documents]


# preprocessed_documents = [' '.join(doc) for doc in tokenized_documents]

# Calculate TF-IDF
# Create a TF-IDF vectorizer with stricter constraints
# tfidf_vectorizer = TfidfVectorizer(max_features=5000, min_df=50, max_df=0.2,norm='l2', use_idf=True, smooth_idf=True)
# tfidf_vectorizer = TfidfVectorizer(max_features=5000, min_df=100, max_df=0.5)
tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(documents)

# tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
print(tfidf_matrix)
# Save TF-IDF Vectorizer using joblib
joblib.dump(tfidf_vectorizer, 'DS1Files/dataset1_tfidf_vectorizer/tfidf_vectorizer.pkl')

# Save TF-IDF Matrix
joblib.dump(tfidf_matrix, 'DS1Files/dataset1_tfidf_vectorizer/tfidf_matrix.pkl')

print('Successfully end')
