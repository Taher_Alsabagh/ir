import pandas as pd
import json
import matching
import sys

def calculate_precision_recall(retrieved_docs, relevant_docs):
    num_retrieved = len(retrieved_docs)
    num_relevant = len(relevant_docs)
    intersection = set(retrieved_docs) & set(relevant_docs)
    num_intersection = len(intersection)
    precision = num_intersection / num_retrieved if num_retrieved > 0 else 0
    recall = num_intersection / num_relevant if num_relevant > 0 else 0
    return precision, recall


def calculate_average_precision(retrieved_docs, relevant_docs):
    num_relevant = len(relevant_docs)
    if num_relevant == 0:
        return 0
    avg_precision = 0.0
    num_hits = 0
    for i, doc in enumerate(retrieved_docs):
        if doc in relevant_docs:
            num_hits += 1
            avg_precision += num_hits / (i + 1)
    avg_precision /= num_relevant
    return avg_precision

def calculate_reciprocal_rank(retrieved_docs, relevant_docs):
        for rank, doc in enumerate(retrieved_docs, start=1):
            if doc in relevant_docs:
                return 1 / rank
        return 0

def evalDS1():
    file_path = 'F:/IT/سنة خامسة/IR/عملي/IR Project DataSets/lotte/writing/dev/qas.forum.jsonl'
    data = []
    # k = 10
    avg_precision = 0
    avg_recall = 0
    avg_ap = 0
    avg_rr = 0
    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            data.append(json.loads(line))
        queries = pd.DataFrame(data)
    # queries = queries.head(5)
    
    for index, row in queries.iterrows():
        precision, recall = 0, 0
        retrieved_docs = matching.getResults('dataset1', row['query'])
        expectedDocs = row['answer_pids']
        precision , recall = calculate_precision_recall(retrieved_docs, expectedDocs)


        avg_precision += precision
        avg_recall += recall
 
        ap = calculate_average_precision(retrieved_docs, expectedDocs)
        avg_ap += ap
        
        rr = calculate_reciprocal_rank(retrieved_docs, expectedDocs)
        avg_rr += rr

    queryLen = len(queries)
    
    avg_precision /= queryLen
    avg_recall /= queryLen
    avg_ap /= queryLen
    avg_rr /= queryLen
        
    print('DS1 Precision = ', avg_precision)
    print('DS1 Recall = ', avg_recall)
    print('DS1 MAP = ', avg_ap)
    print('DS1 MRR = ', avg_rr)
    print("==============================================")
    
    
evalDS1()

def getExpectedDocIDsForQueryId(queryId):
    file_path = 'F:\IT\سنة خامسة\IR\عملي\other IR projects\MyIRProject\DS2Project\csv\qrels.csv'
    data = pd.read_csv(file_path, encoding='utf-8')
    qrels = pd.DataFrame(data)
    expectedDocIDs = set()
    for index, row in qrels.iterrows():
        if(row['query_id'] == queryId):
            expectedDocIDs.add(row['doc_id']) 
    return list(expectedDocIDs)

def evalDS2():
    file_path = 'F:\IT\سنة خامسة\IR\عملي\other IR projects\MyIRProject\DS2Project\csv\queries.csv'
    data = []
    # k = 10
    avg_precision = 0
    avg_recall = 0
    avg_ap = 0
    avg_rr = 0
    
    data = pd.read_csv(file_path, encoding='utf-8')
    queries = pd.DataFrame(data)

    # queries = queries.head(5)
    
    for index, row in queries.iterrows():
        precision, recall = 0, 0
        precision_at_10 = []
        recall_at_10 = []
        retrieved_docs = matching.getResults('dataset2', row['text'])
        expectedDocs = getExpectedDocIDsForQueryId(row['query_id'])
        precision , recall = calculate_precision_recall(retrieved_docs, expectedDocs)

            
        avg_precision += precision
        avg_recall += recall
 
        ap = calculate_average_precision(retrieved_docs, expectedDocs)
        avg_ap += ap
        
        rr = calculate_reciprocal_rank(retrieved_docs, expectedDocs)
        avg_rr += rr
        

        
    queryLen = len(queries)
    
    avg_precision /= queryLen
    avg_recall /= queryLen
    avg_ap /= queryLen
    avg_rr /= queryLen
        
    print('DS2 Precision = ', avg_precision)
    print('DS2 Recall = ', avg_recall)
    print('DS2 MAP = ', avg_ap)
    print('DS2 MRR = ', avg_rr)

evalDS2()
