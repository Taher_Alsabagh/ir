import pandas as pd
import joblib
import string
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.tokenize import word_tokenize
stop_words = set(stopwords.words('english'))
stemmer = SnowballStemmer("english")
lemmatizer = WordNetLemmatizer()


def loadDS1():
    global documents, document_ids
    data = pd.read_csv("DS1Files/df_docs_preprocessed.csv")
    document_ids = data['doc_id'].tolist()
    documents = data['new_text'].tolist()
    return document_ids, documents

def loadTFIDFFile():
    global tfidf_matrix
    tfidf_matrix = joblib.load('DS1Files/dataset1_tfidf_vectorizer/tfidf_matrix.pkl')
    return tfidf_matrix

def loadTFIDFVectorizer():
    global tfidf_vectorizer
    tfidf_vectorizer = joblib.load('DS1Files/dataset1_tfidf_vectorizer/tfidf_vectorizer.pkl')
    return tfidf_vectorizer

def cleanQuery(query):
    stop_words = set(stopwords.words('english'))
    stemmer = SnowballStemmer("english")
    lemmatizer = WordNetLemmatizer()
    query = query.lower()
    query = " ".join([w for w in query.split() if w.lower() not in stop_words])
    query = query.translate(str.maketrans('', '', string.punctuation))
    tokenized_query = word_tokenize(query)
    lemmatized_query = [lemmatizer.lemmatize(word) for word in tokenized_query]
    stemmed_query = [stemmer.stem(word) for word in lemmatized_query]
    preprocessed_query = ' '.join(stemmed_query)
    # print(preprocessed_query)
    return preprocessed_query
    # return tfidf_vectorizer.transform([preprocessed_query])