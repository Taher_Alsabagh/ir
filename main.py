import tkinter as tk
import matching


class GUI:
    def __init__(self, master):
        self.master = master
        master.title("GUI")
        master.geometry("500x300")
        master.configure(bg="#f0f0f0")

        self.label = tk.Label(master, text="Select Dataset", font=(
            "Arial", 12), bg="#f0f0f0", fg="#333333")
        self.label.pack(pady=5)

        self.dataset_var = tk.StringVar(master)
        self.dataset_var.set("dataset1")

        self.dataset_menu = tk.OptionMenu(
            master, self.dataset_var, "dataset1", "dataset2")
        self.dataset_menu.config(font=("Arial", 10), bg="#ffffff")
        self.dataset_menu.pack(pady=5)

        self.entry_label = tk.Label(master, text="Enter Query", font=(
            "Arial", 12), bg="#f0f0f0", fg="#333333")
        self.entry_label.pack(pady=5)

        self.entry = tk.Entry(master, font=("Arial", 12), width=40,
                              bg="#ffffff", bd=2, relief=tk.GROOVE)  # تكبير حجم الـ input
        self.entry.pack(pady=5, padx=0)

        self.submit_button = tk.Button(master, text="Submit", command=self.submit_handler, font=(
            "Arial", 12), bg="#4CAF50", fg="white", relief=tk.FLAT)
        self.submit_button.pack(pady=10)
        self.submit_button.config(width=10, height=1)
        self.submit_button.bind(
            "<Enter>", lambda e: self.submit_button.config(bg="#45a049"))
        self.submit_button.bind(
            "<Leave>", lambda e: self.submit_button.config(bg="#4CAF50"))

    def submit_handler(self):
        dataset = self.dataset_var.get()
        data = self.entry.get()
        matching.getResults(dataset, data)
        # result = processing.process_data(dataset, data)


def run_gui():
    root = tk.Tk()
    my_gui = GUI(root)
    root.mainloop()


if __name__ == "__main__":
    run_gui()
