import string
import re
import sys
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer, SnowballStemmer
import joblib

# تحميل البيانات
dataset = pd.read_csv("C:\\Users\\ASUS\\Desktop\\Python\\lotte\\writing\\dev\\collection_csv.csv")

docs_data = pd.DataFrame({
    'doc_id': dataset['id'],
    'text': dataset['text'],
}, index=range(len(dataset)))

df_docs = pd.DataFrame(docs_data)
# print(len(df_docs))
# sys.exit()
# df_docs = df_docs.head(5)

# تحميل الموارد لمرة واحدة
stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
stemmer = SnowballStemmer("english")

# دالة لتوحيد النصوص
def lemmatize_text(text):
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(w) for w in text]

# دالة لتنظيف النص وإزالة الرموز غير المفهومة
def clean_text(text):
    # إزالة الرموز غير المفهومة باستخدام تعبيرات منتظمة
    text = re.sub(r'[^\x00-\x7F]+', '', text)
    return text

# Function for data preprocessing
def data_preprocessing(df):
    print("\nStarting preprocessing...")
    
    # Remove non-ASCII characters
    df['new_text'] = df['text'].apply(clean_text)
    print("\nAfter removing non-ASCII characters:")
    # print(df['new_text'])

    # Convert text to lowercase
    df['new_text'] = df['new_text'].str.lower()
    print("\nAfter lowercasing:")
    # print(df['new_text'])

    # Remove stop words
    df['new_text'] = df['new_text'].apply(lambda x: " ".join([w for w in x.split() if w not in stop_words]))
    print("\nAfter removing stop words:")
    # print(df['new_text'])

    # Remove punctuation
    df['new_text'] = df['new_text'].apply(lambda x: x.translate(str.maketrans('', '', string.punctuation)))
    print("\nAfter removing punctuation:")
    # print(df['new_text'])

    # Tokenize text
    df['new_text'] = df['new_text'].apply(word_tokenize)
    print("\nAfter tokenization:")
    # print(df['new_text'])

    # Lemmatize text
    df['new_text'] = df['new_text'].apply(lemmatize_text)
    print("\nAfter lemmatization:")
    # print(df['new_text'])

    # Stem text
    df['new_text'] = df['new_text'].apply(lambda x: [stemmer.stem(y) for y in x])
    print("\nAfter stemming:")
    # print(df['new_text'])

    # Join tokens back into strings
    df['new_text'] = df['new_text'].apply(lambda x: ' '.join(x))
    print("\nAfter joining tokens:")
    # print(df['new_text'])

    print("\nPreprocessing complete.")
    return df

# Preprocess the documents
df_docs_preprocessed = data_preprocessing(df_docs)

print("\ndf_docs_preprocessed DataFrame:")
print(df_docs_preprocessed.head())

# حفظ DataFrame المعالجة إلى ملف CSV
df_docs_preprocessed.to_csv("DS1Files/df_docs_preprocessed.csv", index=False)

print("Preprocessed documents saved to 'DS1Files/df_docs_preprocessed_5.csv'")
