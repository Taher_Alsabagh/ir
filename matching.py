import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import loadDataSet1Files
import loadDataSet2Files
import resultPage
import json
import sys

documents = []
document_ids = []
tfidf_vectorizer = None
tfidf_matrix = None
doc_id_map = None
top_n = 15



def loadDataset1Files():
    global documents, document_ids, tfidf_matrix, tfidf_vectorizer
    document_ids, documents = loadDataSet1Files.loadDS1()
    tfidf_matrix = loadDataSet1Files.loadTFIDFFile()
    tfidf_vectorizer = loadDataSet1Files.loadTFIDFVectorizer()

def loadDataset2Files():
    global documents, document_ids, tfidf_matrix, tfidf_vectorizer, doc_id_map
    document_ids, documents = loadDataSet2Files.loadDS2()
    tfidf_matrix = loadDataSet2Files.loadTFIDFFile()
    tfidf_vectorizer = loadDataSet2Files.loadTFIDFVectorizer()
    doc_id_map = loadDataSet2Files.loadDocIdsMap()
    
    
def matching(query_vector, tfidf_matrix, top_n=10):
    cosine_similarities = cosine_similarity(query_vector, tfidf_matrix).flatten()
    indices = np.argsort(cosine_similarities)[::-1]
    top_indices = indices[:top_n]
    return top_indices
    


    
def ExpectedIdsTeach(query_text):
    queries_file_path = 'F:\IT\سنة خامسة\IR\عملي\other IR projects\MyIRProject\DS2Project\csv\queries.csv'
    queries_df = pd.read_csv(queries_file_path)
    query_id = None
    for index, row in queries_df.iterrows():
        if row['text'] == query_text:
            query_id = row['query_id']
            break
    if query_id is None:
        return None, set()

    qrels_file_path = 'F:\IT\سنة خامسة\IR\عملي\other IR projects\MyIRProject\DS2Project\csv\qrels.csv'
    qrels_df = pd.read_csv(qrels_file_path, encoding='utf-8')
    related_documents = qrels_df[qrels_df['query_id'] == int(query_id)]
    expected_ids = set()
    for index, row in related_documents.iterrows():
        expected_ids.add(row['doc_id'])

    return query_id, list(expected_ids)



def ExpectedIdsLotte(query):
    expected_results = []
    file_path = 'F:/IT/سنة خامسة/IR/عملي/IR Project DataSets/lotte/writing/dev/qas.forum.jsonl'
    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            obj = json.loads(line)
            if query in obj['query']:
                expected_results.append(obj)

    answer_pids_list = [
        pid for obj in expected_results for pid in obj['answer_pids']]
    # print(answer_pids_list)
    return answer_pids_list

# Function to calculate evaluation metrics
# print(ExpectedIdsLotte("The Rules of Writing"))

def precision_at_k_fun(recommended, relevant, k):
    # print(len(recommended), recommended)
    # print(len(relevant), relevant)
    recommended_at_k = recommended[:k]
    relevant_set = set(relevant)
    hits = len([doc for doc in recommended_at_k if doc in relevant_set])
    return hits / k

def recall_fun(recommended, relevant):
    relevant_set = set(relevant)
    hits = len([doc for doc in recommended if doc in relevant_set])
    return hits / len(relevant_set)

def average_precision_fun(recommended, relevant):
    relevant_set = set(relevant)
    hits = 0
    sum_precisions = 0.0
    for i, doc in enumerate(recommended):
        if doc in relevant_set:
            hits += 1
            sum_precisions += hits / (i + 1)
    return sum_precisions / len(relevant_set) if relevant_set else 0.0


def reciprocal_rank_fun(recommended, relevant):
    for i, doc in enumerate(recommended):
        if doc in relevant:
            return 1 / (i + 1)
    return 0.0

def calculate_metrics(expected_results, q_results, k=10):
    # precision_at_k = len(set(expected_results) & set(q_results[:k])) / k
    precision_at_k = precision_at_k_fun(expected_results, q_results, k)
    # recall = len(set(expected_results) & set(q_results)) / len(expected_results)
    recall = recall_fun(expected_results, q_results)
    
    # average_precision = 0.0
    # num_correct = 0
    # for i, doc_id in enumerate(q_results):
    #     if doc_id in expected_results:
    #         num_correct += 1
    #         average_precision += num_correct / (i + 1)
    # average_precision /= len(expected_results)
    
    average_precision = average_precision_fun(expected_results, q_results)
    
    # reciprocal_rank = 0.0
    # for i, doc_id in enumerate(q_results):
    #     if doc_id in expected_results:
    #         reciprocal_rank = 1 / (i + 1)
    #         break

    reciprocal_rank = reciprocal_rank_fun(expected_results, q_results)
    return precision_at_k, recall, average_precision, reciprocal_rank

# Function to recall expected IDs for Teach dataset and calculate metrics



def GetPrecisionAnt(query, q_results):
    query_id, expected_ids = ExpectedIdsTeach(query)
    if query_id is None:
        print(f"Query text '{query}' not found in queries file.")
        return 0, 0, 0, 0  # Return zero metrics in case of missing query

    # print("Expected IDs:", expected_ids)
    precision_at_k, recall, average_precision, reciprocal_rank = calculate_metrics(list(expected_ids), q_results)
    
    print("Precision@10 for Teach dataset:", precision_at_k)
    print("Recall for Teach dataset:", recall)
    print("Mean Average Precision (MAP) for Teach dataset:", average_precision)
    print("Mean Reciprocal Rank (MRR) for Teach dataset:", reciprocal_rank)
    return precision_at_k, recall, average_precision, reciprocal_rank


def GetPrecisionLotte(query, q_results):
    expected_ids = ExpectedIdsLotte(query)
    if not expected_ids:
        print(f"No expected IDs found for query '{query}'.")
        return 0, 0, 0, 0  # Return zero metrics in case of missing expected results

    # print("Expected IDs:", expected_ids)
    precision_at_k, recall, average_precision, reciprocal_rank = calculate_metrics(expected_ids, q_results)
    
    print("Precision@10 for Lotte dataset:", precision_at_k)
    print("Recall for Lotte dataset:", recall)
    print("Mean Average Precision (MAP) for Lotte dataset:", average_precision)
    print("Mean Reciprocal Rank (MRR) for Lotte dataset:", reciprocal_rank)
    return precision_at_k, recall, average_precision, reciprocal_rank

# Function to get results based on dataset and query

def getOriginalDocsDS1(docIDs):
    original_dataset = pd.read_csv("F:\IT\سنة خامسة\IR\عملي\IR Project DataSets\lotte\writing\dev\collection.csv", encoding='utf-8')
    original_docs = original_dataset[original_dataset['doc_id'].isin(docIDs)]
    return original_docs
    
    
def getOriginalDocsDS2(docIDs):
    original_dataset = pd.read_csv("F:\IT\سنة خامسة\IR\عملي\other IR projects\MyIRProject\DS2Project\csv\collection.csv", encoding='utf-8')
    original_docs = original_dataset[original_dataset['id'].isin(docIDs)]
    original_docs = original_docs.rename(columns={'id': 'doc_id'})
    return original_docs
    
def getResults(dataset, query):
    global tfidf_vectorizer, tfidf_matrix, top_n
    if(dataset == 'dataset1'):
        loadDataset1Files()
        proccessedQuery = loadDataSet1Files.cleanQuery(query)
        # print(proccessedQuery)
        # print(tfidf_vectorizer)
        queryVector = tfidf_vectorizer.transform([proccessedQuery])
    else:
        loadDataset2Files()
        proccessedQuery = loadDataSet2Files.cleanQuery(query)
        queryVector = tfidf_vectorizer.transform([proccessedQuery])
        
    docIDs = matching(queryVector, tfidf_matrix, top_n)
    
    if(dataset == 'dataset2'):
        docIDs = [doc_id_map[idx] for idx in docIDs]
    # sys.exit("donr")
    # return docIDs
    if(dataset == 'dataset1'):
        # lotte_metrics = GetPrecisionLotte(query, docIDs)
        originalDocs = getOriginalDocsDS1(docIDs)
        # print(originalDocs)
    else:
        # teach_metrics = GetPrecisionAnt(query, docIDs)
        originalDocs = getOriginalDocsDS2(docIDs)
    resultPage.display_numbers(originalDocs)
    # print("uuu")
    # print(docIDs)
    
# getResults('dataset1', 'The Rules of Writing')
# getResults('dataset2', 'What causes severe swelling and pain in the knees?')
    # if(dataset == 'dataset1'):
    #     lotte_metrics = GetPrecisionLotte(query, docIDs)
    # else:
    #     teach_metrics = GetPrecisionAnt(query, docIDs)
    # resultPage.display_numbers(docIDs)
    # return docIDs, teach_metrics, lotte_metrics