import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
import joblib
from nltk.tokenize import word_tokenize

# تحميل الكائنات المحفوظة
count = joblib.load('DS1Files/dataset1_tfidf_vectorizer/count_vectorizer.pkl')
tfidf_transformer = joblib.load('DS1Files/dataset1_tfidf_vectorizer/tfidf_transformer.pkl')
tf_idf_vector = joblib.load('DS1Files/dataset1_tfidf_vectorizer/tfidf_matrix.pkl')
document_ids = joblib.load('DS1Files/dataset1_tfidf_vectorizer/document_ids.pkl')

# معالجة استعلام جديد
def process_query(query):
    query = query.lower()
    query = word_tokenize(query)
    query = ' '.join(query)
    
    query_word_count = count.transform([query])
    query_tfidf = tfidf_transformer.transform(query_word_count)
    return query_tfidf

# تطبيق الاستعلام الجديد
new_query = "rule write"
query_tfidf = process_query(new_query)

# حساب التشابه الكوني بين الاستعلام والوثائق
from sklearn.metrics.pairwise import cosine_similarity
cosine_similarities = cosine_similarity(query_tfidf, tf_idf_vector).flatten()

# الحصول على أعلى 25 وثيقة متشابهة
top_n = 25
top_indices = cosine_similarities.argsort()[-top_n:][::-1]

# عرض النتائج
for index in top_indices:
    print(f"Document ID: {document_ids[index]} | Similarity: {cosine_similarities[index]:.4f}")

print("Query processing completed.")
