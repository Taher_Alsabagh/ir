import pandas as pd
import joblib
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer, SnowballStemmer
import string
import re

stop_words = set(stopwords.words('english'))
stemmer = SnowballStemmer("english")
lemmatizer = WordNetLemmatizer()

def loadDS2():
    global documents, document_ids
    data = pd.read_csv("DS2Files/df_docs_preprocessed.csv")
    document_ids = data['doc_id'].tolist()
    documents = data['new_text'].tolist()
    return document_ids, documents

def loadTFIDFFile():
    global tfidf_matrix
    tfidf_matrix = joblib.load('DS2Files/dataset2_tfidf_vectorizer/tfidf_matrix.pkl')
    return tfidf_matrix

def loadTFIDFVectorizer():
    global tfidf_vectorizer
    tfidf_vectorizer = joblib.load('DS2Files/dataset2_tfidf_vectorizer/tfidf_vectorizer.pkl')
    return tfidf_vectorizer

def loadDocIdsMap():
    global doc_id_map
    doc_id_map = joblib.load('DS2Files/dataset2_tfidf_vectorizer/doc_id_map.pkl')
    return doc_id_map


def remove_urls(text):
    url_pattern = re.compile(r'https?://\S+|www\.\S+')
    return url_pattern.sub(r'', text)

def clean_text(text):
    text = re.sub(r'[^\x00-\x7F]+', '', text)
    return text

def cleanQuery(query):
    if not isinstance(query, str):
        query = ""

    query = remove_urls(query)
    query = clean_text(query)
    query = query.lower()
    query = " ".join([w for w in query.split() if w.lower() not in stop_words])
    query = query.translate(str.maketrans('', '', string.punctuation))
    tokenized_query = word_tokenize(query)
    lemmatized_query = [lemmatizer.lemmatize(word) for word in tokenized_query]
    stemmed_query = [stemmer.stem(word) for word in lemmatized_query]
    preprocessed_query = ' '.join(stemmed_query)
    print(preprocessed_query)
    return preprocessed_query
    # return tfidf_vectorizer.transform([preprocessed_query])