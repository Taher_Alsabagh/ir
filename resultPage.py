import tkinter as tk
from tkinter import ttk

def display_numbers(numbers):
    # Create the main window
    root = tk.Tk()
    root.title("Number Display")
    root.geometry("300x400")  # Set the size of the window

    # Create a frame to hold the listbox
    frame = ttk.Frame(root, padding="20")
    frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))

    # Create a scrollbar
    scrollbar = ttk.Scrollbar(frame, orient=tk.VERTICAL)
    scrollbar.grid(row=0, column=1, sticky=(tk.N, tk.S))

    # Create a listbox to display the numbers with larger font
    listbox = tk.Listbox(frame, yscrollcommand=scrollbar.set, font=("Helvetica", 14), bg="#f0f0f0", fg="#333")
    listbox.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))

    # Configure the scrollbar
    scrollbar.config(command=listbox.yview)

    # Add numbers to the listbox
    for number in numbers:
        listbox.insert(tk.END, str(number))

    # Configure resizing behavior
    frame.columnconfigure(0, weight=1)
    frame.rowconfigure(0, weight=1)

    # Style configurations for better look
    style = ttk.Style()
    style.configure("TFrame", background="#f8f8f8")
    style.configure("TScrollbar", background="#d4d4d4")
    
    # Run the application
    root.mainloop()

# Example usage
# numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# display_numbers(numbers)
