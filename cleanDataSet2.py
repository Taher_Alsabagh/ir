import string
import re
import sys
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer, SnowballStemmer
import joblib

# تحميل البيانات
dataset = pd.read_csv("F:\IT\سنة خامسة\IR\عملي\other IR projects\MyIRProject\DS2Project\csv\collection.csv", encoding="utf-8")

docs_data = pd.DataFrame({
    'doc_id': dataset['id'],
    'text': dataset['text'],
}, index=range(len(dataset)))

df_docs = pd.DataFrame(docs_data)

# تحميل الموارد لمرة واحدة
stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
stemmer = SnowballStemmer("english")

# دالة لإزالة الروابط
def remove_urls(text):
    url_pattern = re.compile(r'https?://\S+|www\.\S+')
    return url_pattern.sub(r'', text)

# دالة لتوحيد النصوص
def lemmatize_text(text):
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(w) for w in text]

def clean_text(text):
    text = re.sub(r'[^\x00-\x7F]+', '', text)
    return text

# Function for data preprocessing
def data_preprocessing(df):
    print("\nStarting preprocessing...")

    # Convert non-string values to empty strings
    df['text'] = df['text'].apply(lambda x: x if isinstance(x, str) else "")

    # Remove URLs
    df['new_text'] = df['text'].apply(remove_urls)
    print("\nAfter removing URLs:")
    # print(df['new_text'])

    # Remove non-ASCII characters
    df['new_text'] = df['new_text'].apply(clean_text)
    print("\nAfter removing non-ASCII characters:")
    # print(df['new_text'])

    # Convert text to lowercase
    df['new_text'] = df['new_text'].str.lower()
    print("\nAfter lowercasing:")
    # print(df['new_text'])

    # Remove stop words
    df['new_text'] = df['new_text'].apply(lambda x: " ".join([w for w in x.split() if w not in stop_words]))
    print("\nAfter removing stop words:")
    # print(df['new_text'])

    # Remove punctuation
    df['new_text'] = df['new_text'].apply(lambda x: x.translate(str.maketrans('', '', string.punctuation)))
    print("\nAfter removing punctuation:")
    # print(df['new_text'])

    # Tokenize text
    df['new_text'] = df['new_text'].apply(word_tokenize)
    print("\nAfter tokenization:")
    # print(df['new_text'])

    # Lemmatize text
    df['new_text'] = df['new_text'].apply(lemmatize_text)
    print("\nAfter lemmatization:")
    # print(df['new_text'])

    # Stem text
    df['new_text'] = df['new_text'].apply(lambda x: [stemmer.stem(y) for y in x])
    print("\nAfter stemming:")
    # print(df['new_text'])

    # Join tokens back into strings
    df['new_text'] = df['new_text'].apply(lambda x: ' '.join(x))
    print("\nAfter joining tokens:")
    # print(df['new_text'])

    print("\nPreprocessing complete.")
    return df

# Preprocess the documents
df_docs_preprocessed = data_preprocessing(df_docs)

print("\ndf_docs_preprocessed DataFrame:")
print(df_docs_preprocessed.head())

# حفظ DataFrame المعالجة إلى ملف CSV
df_docs_preprocessed.to_csv("DS2Files/df_docs_preprocessed.csv", index=False)

print("Preprocessed documents saved to 'DS2Files/df_docs_preprocessed.csv'")
